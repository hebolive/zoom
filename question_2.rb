class Question2
  def self.hello_world
  end

  def self.hi
  end

  (Question2.methods - Object.methods).each { |method|
    current = method(method)
    parameters = current.parameters
    if (parameters.any?)
      define_singleton_method(method.to_sym) { |parameters|
        puts(method)
        current.call(parameters)
      }
    else
      define_singleton_method(method.to_sym) {
        puts(method)
        current.call
      }
    end
  }
end

Question2.hello_world
Question2.hi
