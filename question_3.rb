def add(x, y)
  x + y
end

result = add(1,2)
raise "adding 1 to 2 should be 3. The result was #{result}" unless result == 3

result = add(1,-2)
raise "adding 1 to -2 should be -1. The result was #{result}" unless result == -1

result = add(0,-3)
raise "adding 0 to -3 should be -3. The result was #{result}" unless result == -3

puts 'no error found'
