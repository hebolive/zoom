def count_repeated(array)
  result = {}
  array.each { |letter| result[letter] = 1 + (result[letter] || 0) }
  result
end

puts count_repeated(%w(a a b))
puts count_repeated(%w(x x x y w))
