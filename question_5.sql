-- Give me the name and age of all employees.
select name, age
from users;

-- Give me the name and organization of all employees.
select u.name, o.name
from users u
left join organizations o on o.id = u.organization_id;

-- Give me the oldest person at each organization
select u.name, o.name
from users u
  left join organizations o on o.id = u.organization_id
where
  (u.age, organization_id) in (
    select max(age), organization_id
    from users
    GROUP BY organization_id
  );

-- Find the first person who joined each company.
select u.name, o.name
from users u
  left join organizations o on o.id = u.organization_id
where
  (meta ->> 'joined', organization_id) in (
    select min(meta ->> 'joined'), organization_id
    from users
    GROUP BY organization_id
  );

-- Give me the name and comment of all employees.
select u.name, meta ->> 'comment'
from users u;
